# MecSystem TCC - Mobile

Um software para tornar mais rápido e prático o acesso aos seus registros de ponto.

Agora você não precisa mais se preocupar com os comprovantes de papel, todos seus registros e comprovantes estão armazenados em nuvem.